import 'package:flutter/material.dart';
import 'second_route_nav2.dart';

class FirstRouteNav2 extends StatelessWidget {
  const FirstRouteNav2({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Route Nav Type 2'),
        backgroundColor: Colors.amberAccent,
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Colors.amberAccent),
          child: const Text('Go to Second Route Type 2'),
          onPressed: () {
            Navigator.pushNamed(context, '/second_view');
            ;
          },
        ),
      ),
    );
  }
}

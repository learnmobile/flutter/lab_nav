import 'package:flutter/material.dart';
import 'second_route_nav3.dart';
import 'package:get/get.dart';

class FirstRouteNav3 extends StatelessWidget {
  const FirstRouteNav3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Route Nav Type 3'),
        backgroundColor: Colors.indigoAccent,
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Colors.indigoAccent),
          child: const Text('Go to Second Route Type 3'),
          onPressed: () {
            Get.to(() => SecondRouteNav3());
          },
        ),
      ),
    );
  }
}

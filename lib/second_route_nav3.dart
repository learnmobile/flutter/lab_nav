import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SecondRouteNav3 extends StatelessWidget {
  const SecondRouteNav3({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Route Route Nav Type 3'),
        backgroundColor: Colors.indigoAccent,
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Colors.indigoAccent),
          onPressed: () {
            Get.back();
          },
          child: const Text('Go back to First Route Nav Type 3!'),
        ),
      ),
    );
  }
}

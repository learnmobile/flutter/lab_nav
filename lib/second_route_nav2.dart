import 'package:flutter/material.dart';

class SecondRouteNav extends StatelessWidget {
  const SecondRouteNav({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Second Route Nav Type 2'),
        backgroundColor: Colors.amberAccent,
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(backgroundColor: Colors.amberAccent),
          onPressed: () {
            Navigator.pop(context);
            ;
          },
          child: const Text('Go back to First Route Nav Type 2!'),
        ),
      ),
    );
  }
}

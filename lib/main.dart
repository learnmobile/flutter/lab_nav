import 'package:flutter/material.dart';
import 'package:lab6_nav/first_route_nav2.dart';
import 'package:lab6_nav/first_route_nav3.dart';
import 'package:lab6_nav/second_route_nav2.dart';
import 'first_route_nav1.dart';
import 'first_route_nav2.dart';
import 'package:get/get.dart';

// Navigation # 1: With Push And Pop
/* void main() {
  runApp(const MaterialApp(
    title: 'Navigation With Push & Pop',
    home: FirstRoute(),
  ));
} */

// Navigation # 2: With Routes

/* void main() {
  runApp(MaterialApp(
    title: 'Navigation With With Routes',
    initialRoute: '/',
    routes: {
      '/': (context) => const FirstRouteNav2(),
      '/second_view': (context) => const SecondRouteNav()
    },
  ));
} */

// Navigation # 3: With GetMaterialApp

/* void main() {
  runApp(const GetMaterialApp(
    title: 'Navigation With GetMaterialApp',
    home: FirstRouteNav3(),
  ));
} */
